
## Getting started

The project aims following rules:

You have to create a admin user

Admin have to authenticate himself

With the token
- Admin can create employees
- Admin can create restaurant
- Admin can upload menu for restaurant
 
Employee have to authenticate himself

With the token 
- Employee can not create restaurant
- Employee can not upload menu for restaurant
- Employee can see the current day menu
- Employee can vote for restaurant menu
- Employee can see winner restaurant

To start project, run:

```
docker-compose up
```

The API will then be available at [http://0.0.0.0:8000](http://0.0.0.0:8000).

The project follows the PEP8 rules and you can see sufficient logging through
```
docker-compose logs
```

You have to add token in header like bellow
```
Authorization: Token {token}
```

To create an admin user, open shell inside the container and run:

```
python manage.py createsuperuser
```

### API List:
| Method     | URL                       | Description                                                                                                                  |
|------------|---------------------------|------------------------------------------------------------------------------------------------------------------------------|
| GET        | admin/                    | Admin Login                                                                                                                  |
| POST       | api/user/                 | Create User                                                                                                                  |
| POST       | api/auth/login/           | User Login                                                                                                                   |
| POST       | api/auth/logout/          | User Logout                                                                                                                  |
| GET/UPDATE | api/user/me/              | User Account View                                                                                                            |
| GET/POST   | api/restaurant/           | Create and retrieve restaurants                                                                                              |
| GET/POST   | api/restaurant/{id}       | Retrieve restaurant by id                                                                                                    |
| GET/POST   | api/restaurant/menu/      | Create and retrieve restaurant menus. Add serve_date or restaurant_id params to fetch menus of a specific date or restaurant |
| GET/POST   | api/restaurant/menu/{id}  | Retrieve restaurant menu by id                                                                                               |
| POST       | api/restaurant/vote/      | Create vote for menus                                                                                                        |
| GET        | api/restaurant/winner/    | Retrieve winner restaurant                                                                                                    |



To run the tests, run:

```
docker-compose run app sh -c "python manage.py test"
```


